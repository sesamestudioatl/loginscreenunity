﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json;
using Quobject.EngineIoClientDotNet.Client.Transports;
using Quobject.EngineIoClientDotNet.ComponentEmitter;
using Quobject.EngineIoClientDotNet.Modules;
using Quobject.EngineIoClientDotNet.Parser;
using Socket = Quobject.SocketIoClientDotNet.Client.Socket;


public class AccountManager : MonoBehaviour{
    static string username = "";
    static string password = "";

    static AckCreateAccount createAccountResponse;
    static AckLogin loginResponse;

    public static void registerAckCreateAccount(Socket socket)
    {
        socket.On("ackCreateAccount", (data) =>
        {
            string str = data.ToString();
            createAccountResponse = JsonConvert.DeserializeObject<AckCreateAccount>(str);

            SocketIOClient.PacketLog("Account creation: " + createAccountResponse.message.ToString());
            PopupUtil.showMessage(createAccountResponse.message.ToString());
        });

    }

    public static void registerAckLogin(Socket socket)
    {
        socket.On("ackLogin", (data) =>
        {
            string str = data.ToString();
            loginResponse = JsonConvert.DeserializeObject<AckLogin>(str);

            SocketIOClient.PacketLog("Account Login: " + loginResponse.message.ToString());
            PopupUtil.showMessage(loginResponse.message.ToString());
            if (loginResponse.status)
            {
                SceneChanger.authedClient = true;
            }
        });
    }

    public static string sendCreateAccountRequest(Socket socket)
    {
        if (username != "" && password != "")
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["name"] = username;
            data["pass"] = Hasher.Hash(password);
            socket.Emit("createAccount", JsonConvert.SerializeObject(data));
            return "";
        }
        PopupUtil.showMessage("Enter a user and password");
        return "Username or password field is blank";
    }

    public static string sendLoginRequest(Socket socket)
    {
        if (username != "" && password != "")
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["name"] = username;
            data["pass"] = Hasher.Hash(password);
            socket.Emit("login", JsonConvert.SerializeObject(data));
            return "";
        }
        PopupUtil.showMessage("Enter a user and password");
        return "Username or password field is blank";
    }

    public static void setUserNameField(string name)
    {
        username = name;
    }
    
    public static void setPasswordField(string pass)
    {
        password = pass;
    }


}
