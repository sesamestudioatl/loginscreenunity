﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupUtil : MonoBehaviour {
    Text text;
    static string popupMsg;
    static bool spawnIt = false;
    public GameObject popUpFrame;
    
    void Start()
    {
        text = GetComponentInChildren<Text>();
        popUpFrame.SetActive(false);
    }

    public static void showMessage(string message)
    {
        popupMsg = message;
        spawnIt = true;
    }

    void setText(string message)
    {
        text.text = message;
    }

    public void Hide()
    {
        popUpFrame.SetActive(false);
    }

    void Update()
    {
        if (spawnIt)
        {
            spawnIt = false;
            setText(popupMsg);
            popUpFrame.SetActive(true);
        }
    }
}
