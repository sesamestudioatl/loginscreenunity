﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json;
using Quobject.EngineIoClientDotNet.Client.Transports;
using Quobject.EngineIoClientDotNet.ComponentEmitter;
using Quobject.EngineIoClientDotNet.Modules;
using Quobject.EngineIoClientDotNet.Parser;
using Socket = Quobject.SocketIoClientDotNet.Client.Socket;

public class SocketIOClient : MonoBehaviour
{
	public string serverURL = "http://localhost:3000";
    public string authNameSpace = "/auth";

	protected Socket auth = null;
	protected List<string> packets = new List<string> ();

    protected static List<string> externalPackets = new List<string>();
    public static void PacketLog(string message)
    {   
        externalPackets.Add(message);
    }

	void Start () {
		DoOpen ();

        RegisterForAccountCreationAck();
        RegisterForLoginAck();
	}

    void RegisterForLoginAck()
    {
        AccountManager.registerAckLogin(auth);
    }

    void RegisterForAccountCreationAck()
    {
        AccountManager.registerAckCreateAccount(auth);
    }

    public void SendAccountCreationRequest()
    {
        string error = AccountManager.sendCreateAccountRequest(auth);
        if (error != "")
        {
            packets.Add(error);
        }
    }

    public void SendLoginRequest()
    {
        string error = AccountManager.sendLoginRequest(auth);
        if (error != "")
        {
            packets.Add(error);
        }
    }

	void Update () {
        if (externalPackets.Count > 0)
        {
            int length = externalPackets.Count;
            foreach (var s in externalPackets)
            {
                packets.Add(s);
            }
            externalPackets.RemoveRange(0, length);
        }

        lock (packets)
        {
            if (packets.Count > 0)
            {
                string str = "";
                foreach (var s in packets)
                {
                    str = str + s + "\n";
                }
                Debug.Log(str);
                packets.Clear();
            }
        }

	}

    Manager manager;
	void DoOpen() {
		if (auth == null) {
            manager = new Manager(new System.Uri(serverURL), new IO.Options());
            auth = manager.Socket(authNameSpace);
            auth.Io();
			auth.On (Socket.EVENT_CONNECT, () => {
				lock(packets) {
					// Access to Unity UI is not allowed in a background thread, so let's put into a shared variable
					packets.Add("Socket.IO connected.");
				}
			});
            auth.On("ackConnection", (data) =>
            {
                string str = data.ToString();

                AckConnectionData chat = JsonConvert.DeserializeObject<AckConnectionData>(str);
                packets.Add("Connection Ackd: " + chat.id.ToString());
                SceneChanger.authedClient = true; //Set flag to load login screen
            });
		}
	}

	void DoClose() {
		if (auth != null) {
			auth.Disconnect ();
            auth = null;
            manager.Close();
            manager = null;
		}
	}

    void Destroy()
    {
        DoClose();
    }

    void OnApplicationQuit()
    {
        DoClose();
    }
}
