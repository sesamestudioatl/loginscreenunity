﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextInputLogin : MonoBehaviour {
    public Text userText;
    public InputField passwordText;

    void Update()
    {
        AccountManager.setUserNameField(userText.text);
        AccountManager.setPasswordField(passwordText.text);
    }
}
